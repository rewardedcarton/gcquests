package org.guildcraft.gcquests.object;

import lombok.Getter;

import java.util.concurrent.TimeUnit;

public enum QuestType {

    DAILY(TimeUnit.DAYS.toMillis(1)),
    WEEKLY(TimeUnit.DAYS.toMillis(7)),
    MONTHLY(TimeUnit.DAYS.toMillis(30));

    @Getter private long expireTime;

    QuestType(long expireTime) {
        this.expireTime = expireTime;
    }
}
