package org.guildcraft.gcquests.managers;

import org.bukkit.Bukkit;
import org.guildcraft.gcquests.GCQuests;
import org.guildcraft.gcquests.object.QuestType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class QuestManager {
    private GCQuests plugin;
    private Map<Quest, Long> activeQuests = new HashMap<>();
    private List<Quest> quests = new ArrayList<>();

    public QuestManager(GCQuests plugin) {
        this.plugin = plugin;
        init();
        Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, this::updateQuests, 1L, 1200L);
    }

    private void init() {
        try {
            // load active quest data into map
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateQuests() {
        activeQuests.entrySet().stream().filter(k -> k.getKey().getType().getExpireTime() + System.currentTimeMillis() <= k.getValue()).forEach(k -> {
            // remove quest from active and get a new random one
        });
    }

    public Quest getRandomQuest(QuestType type) {
        List<Quest> quests = this.quests.stream().filter(q -> q.getType() != type).collect(Collectors.toList());
        return quests.get(ThreadLocalRandom.current().nextInt(quests.size()));
    }
}
