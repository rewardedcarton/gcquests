package org.guildcraft.gcquests.quests;

import org.guildcraft.gcquests.object.QuestType;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

public class Quest implements Externalizable {
    private String name;
    private String internalName;
    private QuestType type;

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {

    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

    }
}
