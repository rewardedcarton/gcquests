package org.guildcraft.gcquests.sql;

import lombok.Getter;
import lombok.Setter;
import org.guildcraft.gcquests.GCQuests;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

public class SQLManager {
    private GCQuests plugin;
    @Getter private Connection connection;
    @Getter @Setter private boolean initialized;

    public SQLManager(GCQuests plugin) {
        this.plugin = plugin;
        try {
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() throws Exception {
        if (isInitialized())
            return;
        String driver = "com.mysql.jdbc.Driver";
        String ip = plugin.getConfig().getString("mysql.ip");
        int port = plugin.getConfig().getInt("mysql.port");
        String database = plugin.getConfig().getString("mysql.database");
        String username = plugin.getConfig().getString("mysql.username");
        String password = plugin.getConfig().getString("mysql.password");
        String conn = "jdbc:mysql://" + ip + ":" + port + "/" + database;
        Class.forName(driver);
        this.connection = DriverManager.getConnection(conn, username, password);
        setInitialized(true);
        connection.prepareStatement("CREATE TABLE IF NOT EXISTS `QuestPlayerData` (username CHAR(16), playerData MEDIUMBLOB);").executeUpdate();
    }
}
