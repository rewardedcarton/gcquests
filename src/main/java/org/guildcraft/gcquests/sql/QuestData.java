package org.guildcraft.gcquests.sql;

import org.bukkit.entity.Player;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.HashMap;
import java.util.Map;

public class QuestData implements Externalizable {
    private Map<String, Integer> questData = new HashMap<>();

    public QuestData() {
        
    }

    @Override
    public void writeExternal(ObjectOutput stream) throws IOException {
        /* This is an example of an older method of writing data that I personally used. DataUtils is more efficient.
        stream.writeInt(0);
        stream.writeObject(questData);
        */

        // This is what you should do.
        stream.writeInt(1); // Your version ID, you will want to bump this up once you make some big change. Ideally you should start at 0.
        DataUtils.writeMapToObjectOutput(questData, stream);
    }

    @Override
    public void readExternal(ObjectInput stream) throws IOException, ClassNotFoundException {
        switch (stream.readInt()) {
            case 0: {
                // Protocol for reading version 0 of QuestData. Never ever delete older methods for reading data unless you are starting fresh, like a reset. Just add a new case for older data.
                questData = (Map<String, Integer>) stream.readObject();
                break;
            }
            case 1: {
                // This is how you will want to start from the get-go. Case 0 is an example of what you would need to do if you overhauled how data is stored and bump the version ID up.
                DataUtils.readObjectInputToMap(questData, stream);
                break;
            }
            default: {
                // This will be an unknown version that you probably shouldn't attempt to read, because it will probably be incompatible and break everything.
                break;
            }
        }
    }
}
